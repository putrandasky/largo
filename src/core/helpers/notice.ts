import store from "@/store";
import { Mutations } from "@/store/enums/StoreEnums";

export const setPageInfo = (text: string): void => {
  store.commit(Mutations.SET_PAGEINFO, text);
};
