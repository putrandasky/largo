import { createI18n } from "vue-i18n/index";

const messages = {
  en: {
    reset: "Reset",
    apply: "Apply",
    back: "Back",
    submit: "Submit",
    addNew: "Add New",
    bulkAction: "Bulk Action",
    removeFromWarehouse: "Remove from Warehouse",
    addToWarehouse: "Add to Warehouse",
    import: "Import",
    export: "Export",
    sync: "Sync",
    code: "Code",
    name: "Name",
    date: "Date",
    driver: "Driver",
    item: "Item",
    address: "Address",
    city: "City",
    phone: "Phone",
    qtyMin: "Qty (Min)",
    qtyDefault: "Qty (Default)",
    renewLicense: "Renew License",
    editRole: "Edit Role",
    edit: "Edit",
    print: "Print",
    delete: "Delete",
    close: "Close",
    'List of Categories': "List of Categories",
    'Add New Category': "Add New Category",
    'List of UOM': "List of UOM",
    'Add New UOM': "Add New UOM",
    'List of Items': "List of Items",
    'Add New Item': "Add New Item",
    'List of Warehouse Area': "List of Warehouse Area",
    'List of Bin Location': "List of Bin Location",
    'List of Location Type': "List of Location Type",


  }
};

const i18n = createI18n({
  legacy: false,
  locale: "en",
  globalInjection: true,
  messages,
});

export default i18n;
