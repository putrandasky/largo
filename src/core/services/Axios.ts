import axios, { AxiosPromise, AxiosInstance, AxiosRequestConfig, AxiosResponse, AxiosError } from 'axios'
// import ElMessage  from 'element-plus'
axios.create({
    baseURL: 'https://demo.largo.id/',
    // withCredentials: false
})

axios.interceptors.request.use((config: AxiosRequestConfig) => {
    if (sessionStorage.getItem('token')) {
        config.headers.authorization = sessionStorage.getItem('token')
        // config.headers.user = sessionStorage.getItem('user')
    }
    return config
}, (error: AxiosError) => {
    return Promise.reject(error)
})

axios.interceptors.response.use((res: AxiosResponse) => {
    // const { data } = res
    // console.log(res.data);

    // if (data.status !== 200 && data.status !== 201) {
    //     console.log(data.msg);

    //     ElMessage<any>({
    //         showClose: true,
    //         message: data.meta.msg,
    //         type: 'error'
    //     })
    // }
    return res
}, (error: AxiosError) => {
    return Promise.reject(error)
})
export default axios
export const get = (url: string): AxiosPromise => {
    return axios.get(url)
}
export const post = (url: string, data: any): AxiosPromise => {
    return axios.post(url, data)
}
//https://github.com/ly1994lyy/Vue3.0_Element_ShopManage/blob/master/src/utils/http.ts