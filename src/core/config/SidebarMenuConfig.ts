const SidebarMenuConfig = [
  {
    pages: [
      {
        heading: "Dashboard",
        route: "/dashboard",
        fontIcon: "bi-bar-chart",
      },
    ],
  },
  {
    heading: "inbound",
    route: "/inbound",
    pages: [
      {
        heading: "Documents",
        route: "/inbound/documents",
        fontIcon: "bi-file-arrow-down",
      },
      {
        heading: "Receiving Jobs",
        route: "/inbound/receiving",
        fontIcon: "bi-briefcase",
      },
      {
        heading: "Putaway Logs",
        route: "/inbound/putaway",
        fontIcon: "bi-card-checklist",
      },
    ],
  },
  {
    heading: "outbound",
    route: "/outbound",
    pages: [
      {
        heading: "Documents",
        route: "/outbound/documents",
        fontIcon: "bi-file-arrow-up",
      },
      {
        heading: "Picking Jobs",
        route: "/outbound/picking",
        fontIcon: "bi-upload",
      },
      {
        heading: "Loading Jobs",
        route: "/outbound/loading",
        fontIcon: "bi-download",
      },
    ],
  },
  {
    heading: "operation",
    route: "/operation",
    pages: [
      {
        heading: "Active Inventory",
        route: "/operation/inventory",
        fontIcon: "bi-file-check",
      },
      {
        heading: "Active Locations",
        route: "/operation/locations",
        fontIcon: "bi-pin-map",
      },
      {
        heading: "Cycle Count Jobs",
        route: "/operation/cycle_count",
        fontIcon: "bi-arrow-counterclockwise",
      },
      {
        heading: "Bin Transfer Jobs",
        route: "/operation/bin_transfer",
        fontIcon: "bi-arrow-left-right",
      },
      {
        heading: "Quarantine List",
        route: "/operation/quarantine",
        fontIcon: "bi-card-list",
      },
      {
        heading: "Replenishment",
        route: "/operation/replenishment",
        fontIcon: "bi-plus-square",
      },
      {
        sectionTitle: "Reports",
        route: "/reports",
        fontIcon: "bi-file-text",
        sub: [
          {
            heading: "Material In",
            route: "/reports/material_in",
          },
          {
            heading: "Material Out",
            route: "/reports/material_out",
          },
          {
            heading: "Cycle Count",
            route: "/reports/cycle_count",
          },
          {
            heading: "Item Transfer",
            route: "/reports/item_transfer",
          },
          {
            heading: "Expired Item List",
            route: "/reports/expired_item_list",
          },
        ],
      },
    ],
  },
  {
    heading: "administration",
    route: "/administration",
    pages: [
      {
        sectionTitle: "User Management",
        route: "/management",
        fontIcon: "bi-people",
        sub: [
          {
            heading: "Users",
            route: "/administration/management/users",
          },
          {
            heading: "Roles",
            route: "/administration/management/roles",
          },
        ],
      },
      {
        sectionTitle: "Master Data",
        route: "/master",
        fontIcon: "bi-files",
        sub: [
          {
            sectionTitle: "Items",
            route: "/items",
            sub: [
              {
                heading: "Items Master",
                route: "/administration/master/items/items_master",
              },
              {
                heading: "Items Categories",
                route: "/administration/master/items/items_categories",
              },
              {
                heading: "Unit of Measurements",
                route: "/administration/master/items/uom",
              },
            ],
          },
          {
            sectionTitle: "location",
            route: "/location",
            sub: [
              {
                heading: "Bin Location",
                route: "/administration/master/location/bin",
              },
              {
                heading: "Bin Area",
                route: "/administration/master/location/area",
              },
              {
                heading: "Bin Type",
                route: "/administration/master/location/type",
              },
            ],
          },
          {
            sectionTitle: "Document Type",
            route: "/document_type",
            sub: [
              {
                heading: "Inbound Document",
                route: "/administration/master/document_type/inbound",
              },
              {
                heading: "Outbound Document",
                route: "/administration/master/document_type/outbound",
              },
              {
                heading: "Inbound Document Source",
                route: "/administration/master/document_type/inbound_source",
              },
              {
                heading: "Outbound Document Destination",
                route: "/administration/master/document_type/outbound_destination",
              },
              {
                heading: "Source Destination Type",
                route: "/administration/master/document_type/type",
              },
            ],
          },
        ],
      },
    ],
  },
];
export default SidebarMenuConfig;
