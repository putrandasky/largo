const dummyData = [
    {
        uomCode: "UNIT",
        uomName: "UNIT"
    },
    {
        uomCode: "PCS",
        uomName: "PCS"
    },
    {
        uomCode: "ROLL",
        uomName: "ROLL"
    },
    {
        uomCode: "PC",
        uomName: "PC"
    },
    {
        uomCode: "KG",
        uomName: "KG"
    },
    {
        uomCode: "CS",
        uomName: "CS"
    },
]

export default dummyData;