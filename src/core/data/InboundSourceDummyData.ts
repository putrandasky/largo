const dummyData = [
    {
        code: "VEN-201903-000011",
        name: "PT. PANDAI DAGANG UTAMA",
        address:"Gedung Cyber Lt.10. Jl. Kuningan Barat No.8, kuningan barat, mampang prapatan, Jakarta Selatan DKI Jakarta",
        phone:"08558406158",
        cp:"",
        email:"nia@ptpdu.co.id",
        city:"Jakarta Selatan",
        type:"Vendor"
    },
    {
        code: "VEN-202102-000006",
        name: "LIUS JANTO",
        address:"Jl. Songsi Dalam No. 27 RT 009 RW 006 Tanah Sereal, Tambora, Jakarta Barat",
        phone:"081293311797",
        cp:"",
        email:"3cejkt@gmail.com",
        city:"Jakarta Barat",
        type:"Vendor"
    },
    {
        code: "VEN-202110-000059",
        name: "AHMAD FAUZI",
        address:"JLN GARUDA 28 BIROBULI PALU SELATAN KOTA PALU SULAWESI TENGAH",
        phone:"082188692004",
        cp:"",
        email:"Achmadfauzi1398@gmail.com",
        city:"Palu",
        type:"Vendor"
    },
    
]

export default dummyData;