const dummyData = [
    {
        row: 'A',
        shelfs: [
            {
                rack: 1,
                drawers: [
                    {
                        tag: 'SA1-RWA-RA1-LV1-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 2
                    },
                    {
                        tag: 'SA1-RWA-RA1-LV2-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 4
                    },
                    {
                        tag: 'SA1-RWA-RA1-LV3-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 4
                    },
                    {
                        tag: 'SA1-RWA-RA1-LV4-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 4
                    },
                    {
                        tag: 'SA1-RWA-RA1-LV5-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 2
                    },
                ]
            },
            {
                rack: 2,
                drawers: [
                    {
                        tag: 'SA1-RWA-RA2-LV1-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 2
                    },
                    {
                        tag: 'SA1-RWA-RA2-LV2-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 2
                    },
                    {
                        tag: 'SA1-RWA-RA2-LV3-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 2
                    },
                    {
                        tag: 'SA1-RWA-RA2-LV4-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 2
                    },
                    {
                        tag: 'SA1-RWA-RA2-LV5-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 2
                    },
                ]
            },
            {
                rack: 3,
                drawers: [
                    {
                        tag: 'SA1-RWA-RA3-LV1-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 2
                    },
                    {
                        tag: 'SA1-RWA-RA3-LV2-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 4
                    },
                    {
                        tag: 'SA1-RWA-RA3-LV3-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 4
                    },
                    {
                        tag: 'SA1-RWA-RA3-LV4-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 2
                    },
                    {
                        tag: 'SA1-RWA-RA3-LV5-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 4
                    },
                ]
            },
            {
                rack: 4,
                drawers: [
                    {
                        tag: 'SA1-RWA-RA4-LV1-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 2
                    },
                    {
                        tag: 'SA1-RWA-RA4-LV2-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 2
                    },
                    {
                        tag: 'SA1-RWA-RA4-LV3-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 2
                    },
                    {
                        tag: 'SA1-RWA-RA4-LV4-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 4
                    },
                    {
                        tag: 'SA1-RWA-RA4-LV5-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 4
                    },
                ]
            },
            {
                rack: 5,
                drawers: [
                    {
                        tag: 'SA1-RWA-RA5-LV1-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 4
                    },
                    {
                        tag: 'SA1-RWA-RA5-LV2-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 4
                    },
                    {
                        tag: 'SA1-RWA-RA5-LV3-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 4
                    },
                    {
                        tag: 'SA1-RWA-RA5-LV4-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 4
                    },
                    {
                        tag: 'SA1-RWA-RA5-LV5-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 2
                    },
                ]
            },
            {
                rack: 6,
                drawers: [
                    {
                        tag: 'SA1-RWA-RA6-LV1-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 4
                    },
                    {
                        tag: 'SA1-RWA-RA6-LV2-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 2
                    },
                    {
                        tag: 'SA1-RWA-RA6-LV3-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 2
                    },
                    {
                        tag: 'SA1-RWA-RA6-LV4-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 4
                    },
                    {
                        tag: 'SA1-RWA-RA6-LV5-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 4
                    },
                ]
            },
            {
                rack: 7,
                drawers: [
                    {
                        tag: 'SA1-RWA-RA7-LV1-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 4
                    },
                    {
                        tag: 'SA1-RWA-RA7-LV2-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 4
                    },
                    {
                        tag: 'SA1-RWA-RA7-LV3-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 2
                    },
                    {
                        tag: 'SA1-RWA-RA7-LV4-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 4
                    },
                    {
                        tag: 'SA1-RWA-RA7-LV5-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 4
                    },
                ]
            },
            {
                rack: 8,
                drawers: [
                    {
                        tag: 'SA1-RWA-RA8-LV1-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 4
                    },
                    {
                        tag: 'SA1-RWA-RA8-LV2-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 2
                    },
                    {
                        tag: 'SA1-RWA-RA8-LV3-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 2
                    },
                    {
                        tag: 'SA1-RWA-RA8-LV4-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 2
                    },
                    {
                        tag: 'SA1-RWA-RA8-LV5-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 2
                    },
                ]
            },
            {
                rack: 9,
                drawers: [
                    {
                        tag: 'SA1-RWA-RA9-LV1-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 2
                    },
                    {
                        tag: 'SA1-RWA-RA9-LV2-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 2
                    },
                    {
                        tag: 'SA1-RWA-RA9-LV3-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 2
                    },
                    {
                        tag: 'SA1-RWA-RA9-LV4-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 2
                    },
                    {
                        tag: 'SA1-RWA-RA9-LV5-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 2
                    },
                ]
            },
        ]
    },
    {
        row: 'B',
        shelfs: [
            {
                rack: 1,
                drawers: [
                    {
                        tag: 'SA1-RWB-RA1-LV1-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 2
                    },
                    {
                        tag: 'SA1-RWB-RA1-LV2-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 4
                    },
                    {
                        tag: 'SA1-RWB-RA1-LV3-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 4
                    },
                    {
                        tag: 'SA1-RWB-RA1-LV4-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 4
                    },
                    {
                        tag: 'SA1-RWB-RA1-LV5-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 2
                    },
                ]
            },
            {
                rack: 2,
                drawers: [
                    {
                        tag: 'SA1-RWB-RA2-LV1-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 2
                    },
                    {
                        tag: 'SA1-RWB-RA2-LV2-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 2
                    },
                    {
                        tag: 'SA1-RWB-RA2-LV3-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 2
                    },
                    {
                        tag: 'SA1-RWB-RA2-LV4-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 2
                    },
                    {
                        tag: 'SA1-RWB-RA2-LV5-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 2
                    },
                ]
            },
            {
                rack: 3,
                drawers: [
                    {
                        tag: 'SA1-RWB-RA3-LV1-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 2
                    },
                    {
                        tag: 'SA1-RWB-RA3-LV2-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 4
                    },
                    {
                        tag: 'SA1-RWB-RA3-LV3-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 4
                    },
                    {
                        tag: 'SA1-RWB-RA3-LV4-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 2
                    },
                    {
                        tag: 'SA1-RWB-RA3-LV5-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 4
                    },
                ]
            },
            {
                rack: 4,
                drawers: [
                    {
                        tag: 'SA1-RWB-RA4-LV1-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 2
                    },
                    {
                        tag: 'SA1-RWB-RA4-LV2-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 2
                    },
                    {
                        tag: 'SA1-RWB-RA4-LV3-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 2
                    },
                    {
                        tag: 'SA1-RWB-RA4-LV4-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 4
                    },
                    {
                        tag: 'SA1-RWB-RA4-LV5-BN1',
                        lpn_capacity: 4,
                        lpn_actual: 4
                    },
                ]
            }
        ]
    },
]

export default dummyData;