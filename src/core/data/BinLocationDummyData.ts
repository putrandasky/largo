const dummyData = [
    {
        binName: "Inbound",
        binDescription: "Business to Business",
        binCategory: "",
        binType: "",
        binArea: ""
    },
    {
        binName: "NG",
        binDescription: "NG Location",
        binCategory: "",
        binType: "NG",
        binArea: ""
    },
    {
        binName: "Outbound",
        binDescription: "Outbound Location",
        binCategory: "",
        binType: "Pick Face",
        binArea: ""
    },
    {
        binName: "QC",
        binDescription: "QC Location",
        binCategory: "",
        binType: "QC",
        binArea: ""
    },
    {
        binName: "A-1-3",
        binDescription: "",
        binCategory: "General",
        binType: "Put n Pick",
        binArea: "B2B"
    },
    {
        binName: "A-1-2",
        binDescription: "",
        binCategory: "General",
        binType: "Put n Pick",
        binArea: "B2B"
    },
    {
        binName: "A-1-6",
        binDescription: "",
        binCategory: "General",
        binType: "Put n Pick",
        binArea: "B2B"
    },
    {
        binName: "Warehouse-B2B - MIA",
        binDescription: "",
        binCategory: "General",
        binType: "Put Away",
        binArea: "Warehouse-B2B - MIA"
    },
]

export default dummyData;