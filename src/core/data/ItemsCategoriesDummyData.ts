const dummyData = [
    {
        categoryName: "Cold Room"
    },
    {
        categoryName: "Dry Room"
    },
    {
        categoryName: "General"
    },
    {
        categoryName: "Special"
    },
]

export default dummyData;