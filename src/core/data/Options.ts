const pickingPeriod = [{
    value: 1,
    text: "Weekly",
},
{
    value: 2,
    text: "Monthly",
},
{
    value: 3,
    text: "Yearly",
},
]
const pickingStrategy = [{
    value: 1,
    text: "FIFO",
},
{
    value: 2,
    text: "FEFO",
},
]
const booleanChoices = [
    {
        value: 0,
        text: "All",
    },
    {
        value: 1,
        text: "Yes",
    },
    {
        value: 2,
        text: "No",
    },
]
const documentStatus = [
    {
        value: 'open',
        text: 'Open',
    },
    {
        value: 'close',
        text: 'Close',
    },
    {
        value: 'inprogress',
        text: 'In Progress',
    },
]
const documentLoadingStatus = [
    {
        value: 'cancel',
        text: 'Cancel',
    },
    {
        value: 'open',
        text: 'Open',
    },
    {
        value: 'close',
        text: 'Close',
    },
    {
        value: 'inprogress',
        text: 'In Progress',
    },
]
const documentReceivingStatus = [
    {
        value: 'waiting for tally',
        text: 'Waiting For Tally',
    },
    {
        value: 'tallying in progress',
        text: 'Tallying In Progress',
    },
    {
        value: 'ready to bin',
        text: 'Ready To Bin',
    },
    {
        value: 'putaway in progress',
        text: 'Putaway In Progress',
    },
    {
        value: 'completed',
        text: 'Completed',
    },
]
const documentPickingStatus = [
    {
        value: 'waiting for picking',
        text: 'Waiting For Picking',
    },
    {
        value: 'picking in progress',
        text: 'Picking in Progress',
    },
    {
        value: 'ready to load',
        text: 'Ready to Load',
    },
    {
        value: 'loading in progress',
        text: 'Loading in Progress',
    },
    {
        value: 'picking complete',
        text: 'Picking Complete',
    },
]
const priorities = [
    {
        value: 'NORMAL',
        text: 'NORMAL'
    },
    {
        value: 'HIGH',
        text: 'HIGH'
    }
]
const cycleCountStatus = [
    {
        value: 'ADJUST TO SCAN',
        text: 'ADJUST TO SCAN'
    },
    {
        value: 'NO ADJUSTMENT',
        text: 'NO ADJUSTMENT'
    },
    {
        value: 'WAITING ADJUSTMENT',
        text: 'WAITING ADJUSTMENT'
    },
]
const cycleCountRemark = [
    {
        value: 'EQUAL',
        text: 'EQUAL'
    },
    {
        value: 'POSITIVE',
        text: 'POSITIVE'
    },
    {
        value: 'NEGATIVE',
        text: 'NEGATIVE'
    },
]
const quarantineStatus = [
    {
        value: 'GOOD',
        text: 'GOOD'
    },
    {
        value: 'QUARANTINE',
        text: 'QUARANTINE'
    },
    {
        value: 'NG',
        text: 'NG'
    },
    {
        value: 'DISPOSAL',
        text: 'DISPOSAL'
    },
    {
        value: 'RETURN TO VENDOR',
        text: 'RETURN TO VENDOR'
    },
]

export { pickingPeriod, pickingStrategy, documentStatus, documentReceivingStatus, documentPickingStatus, documentLoadingStatus, booleanChoices, priorities, cycleCountStatus, cycleCountRemark, quarantineStatus };