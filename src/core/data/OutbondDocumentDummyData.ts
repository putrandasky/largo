const dummyData = [
    {
        name: "Sales Order (SO)",
        code: "SO",
    },
    {
        name: "Return to Vendor (RTV)",
        code: "RTV",
    },
    {
        name: "Material Order (MO)",
        code: "MO",
    },
    {
        name: "Material Issue Form (MIF)",
        code: "MIF",
    },
    
]

export default dummyData;