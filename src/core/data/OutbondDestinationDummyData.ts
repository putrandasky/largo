const dummyData = [
    {
        code: "CUS-201902-000002",
        name: "RUS IVANA",
        address:"IVA BEAUTE JL. Jeruk Mas Barat V Taman Kebon Jeruk Blok C3 No. 1",
        phone:"0818990081",
        cp:"",
        email:"",
        city:"Jakarta Barat",
        type:"Customer"
    },
    {
        code: "CUS-201902-000003",
        name: "PT. ECART WEBPORTAL INDONESIA",
        address:"CAPITAL PLACE, lt. 20 dan 21 Jl. Jenderal Gatot Subroto kav. 18",
        phone:"081293311797",
        cp:"",
        email:"",
        city:"Jakarta Selatan",
        type:"Customer"
    },
    {
        code: "CUS-201902-000004",
        name: "PT. JINGDONG INDONESIA PERTAMA",
        address:"(JD.ID) RDTX Tower Lantai 9 Jl. Prof. DR. Satrio Kav E4 No. 6 Mega Kuningan, RT.5/RW.2",
        phone:"08569882890",
        cp:"",
        email:"",
        city:"Jakarta Selatan",
        type:"Customer"
    },
    {
        code: "CUS-201903-000008",
        name: "PT. KOREA INDONESIA CREATIVE TECH AND TRADE",
        address:"Tempo Scan Tower lt.32, #3228 JL. HR. Rasuna Said Kav. 3-4, Kuningan Timur, Setiabudi",
        phone:"",
        cp:"",
        email:"",
        city:"Jakarta Selatan",
        type:"Customer"
    },
    
]

export default dummyData;