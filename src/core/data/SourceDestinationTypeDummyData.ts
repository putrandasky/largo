const dummyData = [
    {
        name: "Vendor",
        code: "Vendor",
    },
    {
        name: "Internal",
        code: "Internal",
    },
    {
        name: "Customer",
        code: "Customer",
    }
    
]

export default dummyData;