const dummyData = [
    {
        typeName: "NG",
        typeDescription: "Damage Area"
    },
    {
        typeName: "Pick Face",
        typeDescription: "Pick Loose Items"
    },
    {
        typeName: "Put Away",
        typeDescription: "Storage Only"
    },
    {
        typeName: "Put n Pick",
        typeDescription: "Open Bin"
    },
    {
        typeName: "QC",
        typeDescription: "Quarantine Area"
    },
    {
        typeName: "TRAY",
        typeDescription: "Tray Area"
    },
]

export default dummyData;