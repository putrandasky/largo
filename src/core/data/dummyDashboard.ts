const dummyInventoryInfo = [
  {
    title: "Active SKU",
    value: "5220",
  },
  {
    title: "Expired Items",
    value: "13",
  },
  {
    title: "Low Items",
    value: "0",
  },
  {
    title: "Non Moving",
    value: "0",
  },
  {
    title: "CC Hold Items",
    value: "0",
  },
  {
    title: "QC Hold Items",
    value: "157",
  },
];
const dummyAreaCap = [
  {
    title: "Area A - General Area",
    value: 32.51,
  },
  {
    title: "Area B - Temporary Area",
    value: 67.34,
  },
  {
    title: "Area C - Cold Storage 1",
    value: 18.78,
  },
  {
    title: "Area D - Cold Storage 2",
    value: 19.56,
  },
  {
    title: "Area E - Preparation Area",
    value: 18.66,
  },
  {
    title: "Area F - High Value Area",
    value: 42.33,
  },
  {
    title: "Area G - Fast Moving Area",
    value: 90.76,
  },
];

export { dummyInventoryInfo, dummyAreaCap };
