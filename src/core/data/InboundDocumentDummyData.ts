const dummyData = [
    {
        name: "SALES RETURN",
        code: "SR",
    },
    {
        name: "Purchase Order",
        code: "PO",
    },
    {
        name: "Laporan Hasil Produksi",
        code: "LHP",
    },
    {
        name: "IMPORT-Doc",
        code: "IM",
    },
    
]

export default dummyData;