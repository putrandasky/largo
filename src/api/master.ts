import axios, { get } from "@/core/services/Axios";

export default {
    getMasterItem({ page = 1, perPage = 10, filter = {} }) {
        let query = ''
        if (Object.keys(filter).length > 0) {
            let filterUsedIndex = 0
            Object.keys(filter).map(function (key, index) {
                if (filter[key]) {
                    filterUsedIndex === 0 ? query += '?' : ''
                    filterUsedIndex > 0 ? query += '&' : ''
                    query += `${key}=${filter[key]}`
                    filterUsedIndex++
                }
            })
        }
        return get(`api/master_data/get_master_item/${page}/${perPage}${query}`)
    },
    getMasterUom(page: any, perPage: any) {
        return get(`api/master_data/get_master_unitOfmeasurement/${page}/${perPage}`)
    },
    getMasterItemCategory(page: any, perPage: any) {
        return get(`api/master_data/get_master_category/${page}/${perPage}`)
    },
    getMasterBinLocation(page: any, perPage: any) {
        return get(`api/master_data/get_master_binLocation/${page}/${perPage}`)
    },
    getMasterLocationArea(page: any, perPage: any) {
        return get(`api/master_data/get_master_area/${page}/${perPage}`)
    },
    getMasterLocationType(page: any, perPage: any) {
        return get(`api/master_data/get_master_type/${page}/${perPage}`)
    },
    getMasterInboundTypes(page: any, perPage: any) {
        return get(`api/master_data/get_master_inboundTypes/${page}/${perPage}`)
    },
    getMasterOutboundTypes(page: any, perPage: any) {
        return get(`api/master_data/get_master_outboundTypes/${page}/${perPage}`)
    },
    getMasterSources(page: any, perPage: any) {
        return get(`api/master_data/get_master_sources/${page}/${perPage}`)
    },
    getMasterDestinations(page: any, perPage: any) {
        return get(`api/master_data/get_master_destinations/${page}/${perPage}`)
    },
    getMasterSourceDesCategories(page: any, perPage: any) {
        return axios.get(`api/master_data/get_master_sourceDesCategories`)
    },
    getMasterUsers(page: any, perPage: any) {
        return get(`api/master_data/get_master_users/${page}/${perPage}`)
    },
    getMasterRoles(page: any, perPage: any) {
        return get(`api/master_data/get_master_roles/${page}/${perPage}`)
    },
    getMasterWarehouse(page: any, perPage: any) {
        return get(`api/master_data/get_master_warehouse/${page}/${perPage}`)
    },

}