import axios, { post } from "@/core/services/Axios";
export default {

    getInboundList(data: any) {
        let dataString = new URLSearchParams(data)
        return post(`api/inbound/getList`, dataString)
    },
    getInboundDetail(data: any) {
        let dataString = new URLSearchParams(data)
        return post(`api/inbound/getDetail`, dataString)
    },
    getReceivingList(data: any) {
        let dataString = new URLSearchParams(data)
        return post(`api/receivings/getList`, dataString)
    },
    getReceivingDetail(data: any) {
        let dataString = new URLSearchParams(data)
        return post(`api/receivings/getDetail`, dataString)
    }
}