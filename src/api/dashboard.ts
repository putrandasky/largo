import { get, post } from "@/core/services/Axios";

export default {
    getKpiTally() {
        return post(`api/dashboard/getKPITallyWidget`, null)
    },
    getKPIPutawayWidget() {
        return post(`api/dashboard/getKPIPutawayWidget`, null)
    },
    getKPIPickingWidget() {
        return post(`api/dashboard/getKPIPickingWidget`, null)
    },
    getKPILoadingWidget() {
        return post(`api/dashboard/getKPILoadingWidget`, null)
    },
    getOperationActivityInbound(param: any) {
        return post(`api/dashboard/getOperationActivityInbound`, param)
    },
    getOperationActivityOutbound(param: any) {
        return post(`api/dashboard/getOperationActivityOutbound`, param)
    },
    getWarehouseCapacity() {
        return post(`api/dashboard/getWarehouseCapacityWidget`, null)
    },
    getInventoryInfo() {
        return post(`api/dashboard/getInventoryInfoWidget`, null)
    }

}