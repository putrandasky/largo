import axios, { post } from "@/core/services/Axios";
export default {

    getOutboundList(data: any) {
        let dataString = new URLSearchParams(data)
        return post(`api/outbound/getList`, dataString)
    },
    getOutboundDetail(data: any) {
        let dataString = new URLSearchParams(data)
        return post(`api/outbound/getDetail`, dataString)
    },
}