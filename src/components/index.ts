import PRow from './layout/Row.vue'
import PCol from './layout/Col.vue'
import PContainer from './layout/Container.vue'
import PCard from './components/Card.vue'
import PButton from './components/Button.vue'
import PBackButton from './components/BackButton.vue'
import PTable from './components/Table.vue'
import PTablePlain from './components/TablePlain.vue'
import PProgress from './components/ProgressBar.vue'
import PSymbol from './components/Symbol.vue'
import PDropdown from './components/Dropdown.vue'
import PEmptyState from './components/EmptyState.vue'
import PLoader from './components/Loader.vue'
import PInput from './components/Input.vue'
import PSelect from './components/Select.vue'
import PBadge from './components/Badge.vue'
import PBtnDropdown from './components/ButtonDropdown.vue'
import PDropdownItem from './components/DropdownItem.vue'
import KTDatatable from './kt-datatable/KTDatatable.vue'
import PFlex from './utilities/Flex.vue'
import PText from './utilities/Text.vue'
import BtnTool from './widgets/BtnTool.vue'
import BtnToolLink from './widgets/BtnToolLink.vue'
import BtnAddNew from './widgets/BtnAddNew.vue'
import BtnSubmit from './widgets/BtnSubmit.vue'
import BtnBack from './widgets/BtnBack.vue'
import SubText from './widgets/SubText.vue'
// import PHeading from './components/Heading'

export default {
  BtnTool,
  BtnToolLink,
  BtnAddNew,
  BtnSubmit,
  BtnBack,
  SubText,
  PRow,
  PCol,
  PContainer,
  PCard,
  PButton,
  PProgress,
  PBackButton,
  PFlex,
  PText,
  PTable,
  PSymbol,
  PDropdown,
  PInput, PSelect, PLoader, PEmptyState,
  KTDatatable, PBtnDropdown, PDropdownItem, PBadge, PTablePlain
}
