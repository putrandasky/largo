import components from './index'

const plugin = {
  install(Vue) {
    for (const prop in components) {
      const component = components[prop]
      Vue.component(component.name, component)
    }
  },
}

export default plugin
