import { Action, Mutation, Module, VuexModule } from "vuex-module-decorators";
import { Actions, Mutations } from "@/store/enums/StoreEnums";

interface State {
  text: string;
}
@Module
export default class PageInfoModule extends VuexModule implements State {
  text = "";

  get pageInfo(): string {
    return this.text;
  }
  @Mutation
  [Mutations.SET_PAGEINFO](payload) {
    this.text = payload;
  }
}
