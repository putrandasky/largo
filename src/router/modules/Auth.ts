
import i18n from '@/core/plugins/i18n'
const { t } = i18n.global
export default
    {
        path: "/",
        component: () => import("@/layout/Auth.vue"),
        children: [
            {
                path: "/sign-in",
                name: "sign-in",
                component: () =>
                    import("@/pages/authentication/basic-flow/SignIn.vue"),
            },
            {
                path: "/sign-up",
                name: "sign-up",
                component: () =>
                    import("@/pages/authentication/basic-flow/SignUp.vue"),
            },
            // {
            //     path: "/password-reset",
            //     name: "password-reset",
            //     component: () =>
            //         import("@/views/crafted/authentication/basic-flow/PasswordReset.vue"),
            // },
        ],
    }