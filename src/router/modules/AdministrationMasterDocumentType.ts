import i18n from '@/core/plugins/i18n'
const { t } = i18n.global
export default {
  path: '/administration/master/document_type',
  redirect: '/administration/master/document_type/inbound/index',
  component: () => import("@/layout/Layout.vue"),
  children: [
    {
      path: "inbound",
      name: "inboundDocument",
      redirect: '/administration/master/document_type/inbound/index',
      component: () => import("@/pages/administration/master-data/document-type/InboundDocument.vue"),
      meta: {
        title: t('Inbound Document Types'),
        subtitle: t('List of inbound document types for used when creating Inbound Documents'),
      },
      children: [
        {
          path: "index",
          name: "inboundDocumentIndex",
          component: () => import("@/pages/administration/master-data/document-type/InboundDocumentIndex.vue"),
        },
        {
          path: "create",
          name: "inboundDocumentCreate",
          component: () => import("@/pages/administration/master-data/document-type/InboundDocumentCreate.vue"),
        },
      ]
    },
    {
      path: "outbound",
      name: "outboundDocument",
      redirect: '/administration/master/document_type/outbound/index',
      component: () => import("@/pages/administration/master-data/document-type/OutboundDocument.vue"),
      meta: {
        title: t('Outbound Document Types'),
        subtitle: t('List of outbound document types for used when creating Outbound Documents'),
      },
      children: [
        {
          path: "index",
          name: "outboundDocumentIndex",
          component: () => import("@/pages/administration/master-data/document-type/OutboundDocumentIndex.vue"),
        },
        {
          path: "create",
          name: "outboundDocumentCreate",
          component: () => import("@/pages/administration/master-data/document-type/OutboundDocumentCreate.vue"),
        },
      ]
    },
    {
      path: "inbound_source",
      name: "inboundSource",
      redirect: '/administration/master/document_type/inbound_source/index',
      component: () => import("@/pages/administration/master-data/document-type/InboundSource.vue"),
      meta: {
        title: t('Sources'),
        subtitle: t('List of item origins or sources from where the item comes from'),
      },
      children: [
        {
          path: "index",
          name: "inboundSourceIndex",
          component: () => import("@/pages/administration/master-data/document-type/InboundSourceIndex.vue"),
        },
        {
          path: "create",
          name: "inboundSourceCreate",
          component: () => import("@/pages/administration/master-data/document-type/InboundSourceCreate.vue"),
        },
      ]
    },
    {
      path: "outbound_destination",
      name: "outboundDestination",
      redirect: '/administration/master/document_type/outbound_destination/index',
      component: () => import("@/pages/administration/master-data/document-type/OutboundDestination.vue"),
      meta: {
        title: t('Destinations'),
        subtitle: t('List of recipients of outgoing items/materials'),
      },
      children: [
        {
          path: "index",
          name: "outboundDestinationIndex",
          component: () => import("@/pages/administration/master-data/document-type/OutboundDestinationIndex.vue"),
        },
        {
          path: "create",
          name: "outboundDestinationCreate",
          component: () => import("@/pages/administration/master-data/document-type/OutboundDestinationCreate.vue"),
        },
      ]
    },
    {
      path: "type",
      name: "sourceDestinationType",
      redirect: '/administration/master/document_type/type/index',
      component: () => import("@/pages/administration/master-data/document-type/SourceDestinationType.vue"),
      meta: {
        title: t('Source / Destination Categories'),
        subtitle: t('List of categories of sources/destinations for the items/materials. This will be used on Master Sources and Master Destinations'),
      },
      children: [
        {
          path: "index",
          name: "sourceDestinationTypeIndex",
          component: () => import("@/pages/administration/master-data/document-type/SourceDestinationTypeIndex.vue"),
        },
        {
          path: "create",
          name: "sourceDestinationTypeCreate",
          component: () => import("@/pages/administration/master-data/document-type/SourceDestinationTypeCreate.vue"),
        },
      ]
    },

  ]
}