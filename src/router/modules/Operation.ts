import i18n from '@/core/plugins/i18n'
const { t } = i18n.global
export default {
    path: '/operation',
    redirect: '/operation/inventory/index',
    component: () => import("@/layout/Layout.vue"),
    children: [
        {
            path: "inventory",
            name: "operationInventory",
            redirect: '/operation/inventory/index',
            component: () => import("@/pages/operation/Inventory.vue"),
            meta: {
                title: t('Active Inventory'),
                subtitle: t('List of all the inventory items / materials'),
            },
            children: [
                {
                    path: "index",
                    name: "operationInventoryIndex",
                    component: () => import("@/pages/operation/InventoryIndex.vue"),
                },
                // {
                //     path: "create",
                //     name: "operationInventoryCreate",
                //     component: () => impor@/pages/operation/InventoryDetail.vuevue"),
                // },
                {
                    path: ":id",
                    name: "operationInventoryDetail",
                    meta: {
                        title: t('Item Detail'),
                        subtitle: t(''),
                    },
                    component: () => import("@/pages/operation/InventoryDetail.vue"),
                }
            ]
        },
        {
            path: "locations",
            name: "operationLocation",
            redirect: '/operation/locations/index',
            component: () => import("@/pages/operation/Location.vue"),
            meta: {
                title: t('Active Location'),
                subtitle: t('Show all bin locations and their contents'),
            },
            children: [
                {
                    path: "index",
                    name: "operationLocationIndex",
                    component: () => import("@/pages/operation/LocationIndex.vue"),
                },
                {
                    path: "visualisation",
                    name: "operationLocationVisualisation",
                    meta: {
                        title: t('Location Visualisation'),
                        subtitle: t(''),
                    },
                    component: () => import("@/pages/operation/LocationVisualisation.vue"),
                },
                {
                    path: "create",
                    name: "operationLocationCreate",
                    component: () => import("@/pages/operation/LocationCreate.vue"),
                },
                {
                    path: ":id",
                    name: "operationLocationDetail",
                    meta: {
                        title: t('Location Detail'),
                        subtitle: t(''),
                    },
                    component: () => import("@/pages/operation/LocationDetail.vue"),
                }
            ]
        },
        {
            path: "cycle_count",
            name: "operationCycleCount",
            redirect: '/operation/cycle_count/index',
            component: () => import("@/pages/operation/CycleCount.vue"),
            meta: {
                title: t('Cycle Count Jobs'),
                subtitle: t('Arrange for cycle count (or stock opname) job to be performed on the floor'),
            },
            children: [
                {
                    path: "index",
                    name: "operationCycleCountIndex",
                    component: () => import("@/pages/operation/CycleCountIndex.vue"),
                },
                {
                    path: "create",
                    name: "operationCycleCountCreate",
                    component: () => import("@/pages/operation/CycleCountCreate.vue"),
                },
                {
                    path: ":id",
                    name: "operationCycleCountDetail",
                    meta: {
                        title: t('Cycle Count Detail'),
                        subtitle: t(''),
                    },
                    component: () => import("@/pages/operation/CycleCountDetail.vue"),
                }
            ]
        },
        {
            path: "bin_transfer",
            name: "operationBinTransfer",
            redirect: '/operation/bin_transfer/index',
            component: () => import("@/pages/operation/BinTransfer.vue"),
            meta: {
                title: t('Bin Transfer Jobs'),
                subtitle: t('Perform bin transfer (or bin-to-bin) using a document and execute the job with RF'),
            },
            children: [
                {
                    path: "index",
                    name: "operationBinTransferIndex",
                    component: () => import("@/pages/operation/BinTransferIndex.vue"),
                },
                {
                    path: "create",
                    name: "operationBinTransferCreate",
                    component: () => import("@/pages/operation/BinTransferCreate.vue"),
                },
                {
                    path: ":id",
                    name: "operationBinTransferDetail",
                    meta: {
                        title: t('Bin Transfer Detail'),
                        subtitle: t(''),
                    },
                    component: () => import("@/pages/operation/BinTransferDetail.vue"),
                }
            ]
        },
        {
            path: "quarantine",
            name: "operationQuarantine",
            redirect: '/operation/quarantine/index',
            component: () => import("@/pages/operation/Quarantine.vue"),
            meta: {
                title: t('Quarantine List'),
            },
            children: [
                {
                    path: "index",
                    name: "operationQuarantineIndex",
                    component: () => import("@/pages/operation/QuarantineIndex.vue"),
                },
                {
                    path: ":id",
                    name: "operationQuarantineDetail",
                    meta: {
                        title: t('Quarantine Inventory'),
                        subtitle: t('List of all the quarantine items / materials'),
                    },
                    component: () => import("@/pages/operation/QuarantineDetail.vue"),
                }
            ]
        },
        {
            path: "replenishment",
            name: "operationReplenishment",
            redirect: '/operation/replenishment/index',
            component: () => import("@/pages/operation/Replenishment.vue"),
            meta: {
                title: t('Replenishment'),
                subtitle: t('This list shows bin transfer (or bin-to-bin) movement performed on RF'),
            },
            children: [
                {
                    path: "index",
                    name: "operationReplenishmentIndex",
                    component: () => import("@/pages/operation/ReplenishmentIndex.vue"),
                }
            ]
        },



    ]
}