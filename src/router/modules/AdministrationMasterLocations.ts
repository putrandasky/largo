import i18n from '@/core/plugins/i18n'
const { t } = i18n.global
export default {
  path: '/administration/master/location',
  redirect: '/administration/master/location/bin/index',
  component: () => import("@/layout/Layout.vue"),
  children: [
    {
      path: "bin",
      redirect: '/administration/master/location/bin/index',
      component: () => import("@/pages/administration/master-data/location/BinLocation.vue"),
      meta: {
        title: t('Bin Locations'),
        subtitle: t('List of all bin locations assigned in the warehouse'),
      },
      children: [
        {
          path: "index",
          name: "locationBinIndex",
          component: () => import("@/pages/administration/master-data/location/BinLocationIndex.vue"),
        },
        {
          path: "create",
          name: "locationBinCreate",
          component: () => import("@/pages/administration/master-data/location/BinLocationCreate.vue"),
        },
      ]
    },
    {
      path: "area",
      redirect: '/administration/master/location/area/index',
      component: () => import("@/pages/administration/master-data/location/BinArea.vue"),
      meta: {
        title: t('Warehouse Area'),
        subtitle: t('List of all separate areas in a warehouse. This is useful to assign specific items/materials for specific areas'),
      },
      children: [
        {
          path: "index",
          name: "locationAreaIndex",
          component: () => import("@/pages/administration/master-data/location/BinAreaIndex.vue"),
        },
        {
          path: "create",
          name: "locationAreaCreate",
          component: () => import("@/pages/administration/master-data/location/BinAreaCreate.vue"),
        }
      ]
    },
    {
      path: "type",
      redirect: '/administration/master/location/type/index',
      component: () => import("@/pages/administration/master-data/location/BinType.vue"),
      meta: {
        title: t('Location Types'),
        subtitle: t('List of different location types to be assigned to bin locations'),
      },
      children: [
        {
          path: "index",
          name: "locationTypeIndex",
          component: () => import("@/pages/administration/master-data/location/BinTypeIndex.vue"),
        },
        {
          path: "create",
          name: "locationTypeCreate",
          component: () => import("@/pages/administration/master-data/location/BinTypeCreate.vue"),
        },
      ]
    },

  ]
}