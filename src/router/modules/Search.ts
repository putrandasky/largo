import i18n from '@/core/plugins/i18n'
const { t } = i18n.global
export default {
    path: '/search',
    redirect: '/',
    component: () => import("@/pages/search/Search.vue"),
    children: [
        {
            path: "sn/:serialNumber",
            name: "searchSerialNumber",
            component: () => import("@/pages/search/SearchSerialNumber.vue"),
            meta: {
                title: t('Search Result'),
                subtitle: t(''),
            },
        }




    ]
}