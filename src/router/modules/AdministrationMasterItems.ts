import i18n from '@/core/plugins/i18n'
const { t } = i18n.global
export default {
  path: '/administration/master/items',
  redirect: '/administration/master/items/items_master/index',
  component: () => import("@/layout/Layout.vue"),
  children: [
    {
      path: "items_master",
      redirect: '/administration/master/items/items_master/index',
      component: () => import("@/pages/administration/master-data/items/ItemsMaster.vue"),
      meta: {
        title: t('Item Master'),
        subtitle: t('Master data of all items/materials'),
      },
      children: [
        {
          path: "index",
          name: "itemsMasterIndex",
          component: () => import("@/pages/administration/master-data/items/ItemsMasterIndex.vue"),

        },
        {
          path: "create",
          name: "itemsMasterCreate",
          component: () => import("@/pages/administration/master-data/items/ItemsMasterCreate.vue"),
        }
      ]
    },
    {
      path: "items_categories",
      redirect: "/administration/master/items/items_categories/index",
      component: () => import("@/pages/administration/master-data/items/ItemsCategories.vue"),
      meta: {
        title: t('Item Categories'),
        subtitle: t('List of all item categories'),
      },
      children: [
        {
          path: "index",
          name: "itemsCategoriesIndex",
          component: () => import("@/pages/administration/master-data/items/ItemsCategoriesIndex.vue"),
        },
        {
          path: "create",
          name: "itemsCategoriesCreate",
          component: () => import("@/pages/administration/master-data/items/ItemsCategoriesCreate.vue"),
        }
      ]
    },
    {
      path: "uom",
      redirect: "/administration/master/items/uom/index",
      component: () => import("@/pages/administration/master-data/items/UnitOfMeasurements.vue"),
      meta: {
        title: t('Unit of Measurements'),
        subtitle: t('List of UoM used for items/materials'),
      },
      children: [
        {
          path: "index",
          name: "itemsUomIndex",
          component: () => import("@/pages/administration/master-data/items/UnitOfMeasurementsIndex.vue"),
        },
        {
          path: "create",
          name: "itemsUomCreate",
          component: () => import("@/pages/administration/master-data/items/UnitOfMeasurementsCreate.vue"),
        }
      ]
    },
  ]
}