import i18n from '@/core/plugins/i18n'
const { t } = i18n.global
export default {
    path: '/user',
    redirect: '/',
    component: () => import("@/layout/Layout.vue"),
    children: [
        {
            path: "change-password",
            name: "changePassword",
            component: () => import("@/pages/user/ChangePassword.vue"),
            meta: {
                title: t('Change Password'),
                subtitle: t(''),
            },
        }




    ]
}