import i18n from '@/core/plugins/i18n'
const { t } = i18n.global
export default {
    path: '/outbound',
    redirect: '/outbound/documents/index',
    component: () => import("@/layout/Layout.vue"),
    children: [
        {
            path: "documents",
            name: "outboundDocuments",
            redirect: '/outbound/documents/index',
            component: () => import("@/pages/outbound/OutboundDocuments.vue"),
            meta: {
                title: t('Outbound Documents'),
                subtitle: t('List of documents for outgoing items/materials'),
            },
            children: [
                {
                    path: "index",
                    name: "outboundDocumentsIndex",
                    component: () => import("@/pages/outbound/OutboundDocumentsIndex.vue"),
                },
                {
                    path: "create",
                    name: "outboundDocumentsCreate",
                    component: () => import("@/pages/outbound/OutboundDocumentsCreate.vue"),
                },
                {
                    path: ":documentId",
                    name: "outboundDocumentsDetail",
                    component: () => import("@/pages/outbound/OutboundDocumentsDetail.vue"),
                }

            ]
        },
        {
            path: "picking",
            name: "outboundPickingJobs",
            redirect: '/outbound/picking/index',
            component: () => import("@/pages/outbound/OutboundPickingJobs.vue"),
            meta: {
                title: t('Picking Jobs'),
                subtitle: t('List of all the picking jobs for outgoing items/materials'),
            },
            children: [
                {
                    path: "index",
                    name: "outboundPickingJobsIndex",
                    component: () => import("@/pages/outbound/OutboundPickingJobsIndex.vue"),
                },
                {
                    path: "create",
                    name: "outboundPickingJobsCreate",
                    component: () => import("@/pages/outbound/OutboundPickingJobsCreate.vue"),
                }
            ]
        },
        {
            path: "loading",
            name: "outboundLoadingJobs",
            redirect: '/outbound/loading/index',
            component: () => import("@/pages/outbound/outboundLoadingJobs.vue"),
            meta: {
                title: t('Loading Jobs ')
            },
            children: [
                {
                    path: "index",
                    name: "outboundLoadingJobsIndex",
                    component: () => import("@/pages/outbound/OutboundLoadingJobsIndex.vue"),
                },
                {
                    path: "create",
                    name: "outboundLoadingJobsCreate",
                    component: () => import("@/pages/outbound/OutboundLoadingJobsCreate.vue"),
                }
            ]
        },



    ]
}