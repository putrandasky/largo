import i18n from '@/core/plugins/i18n'
const { t } = i18n.global
export default {
    path: '/inbound',
    redirect: '/inbound/documents/index',
    component: () => import("@/layout/Layout.vue"),
    children: [
        {
            path: "documents",
            name: "inboundDocuments",
            redirect: '/inbound/documents/index',
            component: () => import("@/pages/inbound/InboundDocuments.vue"),
            meta: {
                title: t('Inbound Documents'),
                subtitle: t('List of documents for incoming items/materials'),
            },
            children: [
                {
                    path: "index",
                    name: "inboundDocumentsIndex",
                    component: () => import("@/pages/inbound/InboundDocumentsIndex.vue"),
                },
                {
                    path: "create",
                    name: "inboundDocumentsCreate",
                    component: () => import("@/pages/inbound/InboundDocumentsCreate.vue"),
                },
                {
                    path: ":documentId",
                    name: "inboundDocumentsDetail",
                    component: () => import("@/pages/inbound/InboundDocumentsDetail.vue"),
                }
            ]
        },
        {
            path: "receiving",
            name: "inboundReceivingJobs",
            redirect: '/inbound/receiving/index',
            component: () => import("@/pages/inbound/InboundReceivingJobs.vue"),
            meta: {
                title: t('Receiving Jobs'),
                subtitle: t('List of receiving jobs for incoming items/materials'),
            },
            children: [
                {
                    path: "index",
                    name: "inboundReceivingJobsIndex",
                    component: () => import("@/pages/inbound/InboundReceivingJobsIndex.vue"),
                },
                {
                    path: "create",
                    name: "inboundReceivingJobsCreate",
                    component: () => import("@/pages/inbound/InboundReceivingJobsCreate.vue"),
                },
                {
                    path: ":documentId",
                    name: "inboundReceivingJobsDetail",
                    component: () => import("@/pages/inbound/InboundReceivingJobsDetail.vue"),
                }
            ]
        },
        {
            path: "putaway",
            name: "inboundPutawayLogs",
            redirect: '/inbound/putaway/index',
            component: () => import("@/pages/inbound/InboundPutawayLogs.vue"),
            meta: {
                title: t('Putaway Logs '),
                subtitle: t('List of all performed putaway (or put-to-bin) transactions'),
            },
            children: [
                {
                    path: "index",
                    name: "inboundPutawayLogsIndex",
                    component: () => import("@/pages/inbound/InboundPutawayLogsIndex.vue"),
                },
                {
                    path: "create",
                    name: "inboundPutawayLogsCreate",
                    component: () => import("@/pages/inbound/InboundPutawayLogsCreate.vue"),
                }
            ]
        },



    ]
}