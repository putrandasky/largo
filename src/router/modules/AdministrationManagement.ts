import i18n from '@/core/plugins/i18n'
const { t } = i18n.global
export default {
  path: '/administration/management',
  redirect: '/administration/management/users/index',
  component: () => import("@/layout/Layout.vue"),
  children: [
    {
      path: "users",
      name: "managementUsers",
      redirect: '/administration/management/users/index',
      component: () => import("@/pages/administration/management/Users.vue"),
      meta: {
        title: t('User Management'),
        subtitle: t('List of Largo app users'),
      },
      children: [
        {
          path: "index",
          name: "managementUsersIndex",
          component: () => import("@/pages/administration/management/UsersIndex.vue"),
        },
        {
          path: "create",
          name: "managementUsersCreate",
          component: () => import("@/pages/administration/management/UsersCreate.vue"),
        }
      ]
    },
    {
      path: "roles",
      name: "managementRoles",
      redirect: '/administration/management/roles/index',
      component: () => import("@/pages/administration/management/Roles.vue"),
      meta: {
        title: t('Role Management'),
        subtitle: t('List of User Roles'),
      },
      children: [
        {
          path: "index",
          name: "managementRolesIndex",
          component: () => import("@/pages/administration/management/RolesIndex.vue"),
        },
        {
          path: "create",
          name: "managementRolesCreate",
          component: () => import("@/pages/administration/management/RolesCreate.vue"),
        }
      ]
    },


  ]
}