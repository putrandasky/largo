import i18n from '@/core/plugins/i18n'
const { t } = i18n.global
export default {
    path: '/reports',
    redirect: '/reports/material_in/index',
    component: () => import("@/layout/Layout.vue"),
    children: [
        {
            path: "material_in",
            name: "materialIn",
            redirect: '/reports/material_in/index',
            component: () => import("@/pages/reports/MaterialIn.vue"),
            meta: {
                title: t('Material In Report'),
                subtitle: t('Report for all incoming items/materials'),
            },
            children: [
                {
                    path: "index",
                    name: "materialInIndex",
                    component: () => import("@/pages/reports/MaterialInIndex.vue"),
                },

            ]
        },
        {
            path: "material_out",
            name: "materialOut",
            redirect: '/reports/material_out/index',
            component: () => import("@/pages/reports/MaterialOut.vue"),
            meta: {
                title: t('Material Out Report'),
                subtitle: t('Report for all outgoing items/materials'),
            },
            children: [
                {
                    path: "index",
                    name: "materialOutIndex",
                    component: () => import("@/pages/reports/MaterialOutIndex.vue"),
                },

            ]
        },
        {
            path: "cycle_count",
            name: "cycleCount",
            redirect: '/reports/cycle_count/index',
            component: () => import("@/pages/reports/CycleCount.vue"),
            meta: {
                title: t('Cycle Count Report'),
                // subtitle: t('List of documents for outgoing items/materials'),
            },
            children: [
                {
                    path: "index",
                    name: "cycleCountIndex",
                    component: () => import("@/pages/reports/CycleCountIndex.vue"),
                },

            ]
        },
        {
            path: "item_transfer",
            name: "itemTransfer",
            redirect: '/reports/item_transfer/index',
            component: () => import("@/pages/reports/ItemTransfer.vue"),
            meta: {
                title: t('Bin Transfer Log'),
                subtitle: t('List of bin transfer (bin-to-bin) operations done on RF aside from assigned Bin Transfer Jobs'),
            },
            children: [
                {
                    path: "index",
                    name: "itemTransferIndex",
                    component: () => import("@/pages/reports/ItemTransferIndex.vue"),
                },

            ]
        },
        {
            path: "expired_item_list",
            name: "expiredItemList",
            redirect: '/reports/expired_item_list/index',
            component: () => import("@/pages/reports/ExpiredItemList.vue"),
            meta: {
                title: t('Expiration Report'),
                // subtitle: t('List of bin transfer (bin-to-bin) operations done on RF aside from assigned Bin Transfer Jobs'),
            },
            children: [
                {
                    path: "index",
                    name: "expiredItemListIndex",
                    component: () => import("@/pages/reports/ExpiredItemListIndex.vue"),
                },
                {
                    path: ":itemcode",
                    name: "expiredItemListDetail",
                    component: () => import("@/pages/reports/ExpiredItemListDetail.vue"),
                },

            ]
        },




    ]
}