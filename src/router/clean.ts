import { createRouter, createWebHashHistory, RouteRecordRaw } from "vue-router";
import store from "@/store";
import { Mutations, Actions } from "@/store/enums/StoreEnums";
import i18n from '@/core/plugins/i18n'
const { t } = i18n.global

import * as routeModule from '@/router/modules/index'

const routes = [
  {
    path: "/",
    redirect: "/dashboard",
    component: () => import("@/layout/Layout.vue"),
    children: [
      {
        path: "/dashboard",
        name: "dashboard",
        component: () => import("@/pages/home/Dashboard.vue"),
        meta: {
          title: t('Dashboard')
        },
      },
      routeModule.Search,

    ],
  },
  routeModule.AdministrationManagement,
  routeModule.AdministrationMasterDocumentType,
  routeModule.AdministrationMasterItems,
  routeModule.AdministrationMasterLocations,
  routeModule.Inbound,
  routeModule.Outbound,
  routeModule.Operation,
  routeModule.Reports,
  routeModule.User,
  routeModule.Auth,
  // {
  //   path: "/",
  //   component:  () => import("@/components/page-layouts/Auth.vue"),
  //   children: [
  //     {
  //       path: "/sign-in",
  //       name: "sign-in",
  //       component:  () => import("@/views/crafted/authentication/basic-flow/SignIn.vue"),
  //     },
  //     {
  //       path: "/sign-up",
  //       name: "sign-up",
  //       component:  () =>  import("@/views/crafted/authentication/basic-flow/SignUp.vue"),
  //     },
  //     {
  //       path: "/password-reset",
  //       name: "password-reset",
  //       component:  () =>   import("@/views/crafted/authentication/basic-flow/PasswordReset.vue"),
  //     },
  //   ],
  // },
  {
    // the 404 routeModule, when none of the above matches
    path: "/404",
    name: "404",
    component: () => import("@/pages/authentication/Error404.vue"),
  },
  {
    path: "/:pathMatch(.*)*",
    redirect: "/404",
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

router.beforeEach(() => {
  // reset config to initial state
  store.commit(Mutations.RESET_LAYOUT_CONFIG);

  // store.dispatch(Actions.VERIFY_AUTH);

  // Scroll page to top on every routeModule change
  setTimeout(() => {
    window.scrollTo(0, 0);
  }, 100);
});

export default router;
