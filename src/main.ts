import { createApp } from "vue";
import App from "./App.vue";

/*
TIP: To get started with clean router change path to @/router/clean.ts.
 */
import router from "./router/clean";
import store from "./store";
import ElementPlus from "element-plus";
import i18n from "@/core/plugins/i18n";

//imports for app initialization
import API from "@/core/services/ApiService";
// import axios, { AxiosInstance } from 'axios'
// import VueAxios from 'vue-axios'
import { initApexCharts } from "@/core/plugins/apexcharts";
import { initInlineSvg } from "@/core/plugins/inline-svg";
import { initVeeValidate } from "@/core/plugins/vee-validate";
import plugin from "@/components/plugin";
import dayjs from 'dayjs'
import WordHighlighter from "vue-word-highlighter";
// import "@/core/plugins/prismjs";
import "bootstrap";

const app = createApp(App);
const { t, te } = i18n.global;
app.use(store);
// app.use(VueAxios, axios)
// app.provide('axios', app.config.globalProperties.axios)
app.use(router);
app.use(ElementPlus);
app.use(plugin);
app.component("WordHighlighter", WordHighlighter);
// app.config.globalProperties.dayjs = dayjs
app.provide('dayjs', dayjs)
app.config.globalProperties.__ = {
  ucfirst(value) {
    return value !== null ? value
      .toLowerCase()
      .split(" ")
      .map((s) => s.charAt(0).toUpperCase() + s.substring(1))
      .join(" ") : '';
  },
  trans(value) {
    if (te(value)) {
      return t(value);
    } else {
      return value;
    }
  },
  dateFormat(value, format = 'DD-MMM-YY') {
    if (value) {
      return dayjs(value).format(format)
    }
    return null
  },
};
// app.config.globalProperties._ = {
//   trans(value) {
//     if (te(value)) {
//       return t(value);
//     } else {
//       return value;
//     }
//   },
// };
// axios.create({
//   baseURL: "http://139.162.36.59/largo/fedev/",
//   headers: {
// "Content-type": "application/x-www-form-urlencoded",
// "Content-type": "application/json",
// "user": "dev_putra",
// "Authorization": "godtp8j6z2a7o93r63oe"
//   },
// });
// axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'
// axios.defaults.headers.common['user'] = 'dev_putra'
// axios.defaults.headers.common['Authorization'] = 'godtp8j6z2a7o93r63oe'
// axios.defaults.withCredentials = true
API.init(app);
initApexCharts(app);
initInlineSvg(app);
initVeeValidate();

app.use(i18n);

app.mount("#app");
