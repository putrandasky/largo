import {
  defineComponent,
  onMounted,
  ref,
  reactive
} from 'vue';
import API from "@/core/services/ApiService";
export default function useMasterSourceType() {
  const dataMasterSourceType = ref([
    {
      sourcecategoryid: '',
      sourcecategoryname: '',
    }
  ])
  const optionsMasterSourceType = ref([
    {
      value: '',
      text: '',
    }
  ])

  const getMasterSourceType = async () => {
    try {
      const res = await API.get('api/master_data/get_master_sourceDesCategories')
      dataMasterSourceType.value = res.data.results
      optionsMasterSourceType.value = dataMasterSourceType.value.map(function (item) {
        return {
          value: item.sourcecategoryid,
          text: item.sourcecategoryname,
        };
      });
    } catch (error: any) {
      console.log(error.response)
    }
  }



  onMounted(() => { });
  return {
    dataMasterSourceType,
    getMasterSourceType,
    optionsMasterSourceType
  }
}
