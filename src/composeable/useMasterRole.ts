import {
  defineComponent,
  onMounted,
  ref,
  reactive
} from 'vue';
import API from "@/core/services/ApiService";
export default function useMasterRole() {
  const dataMasterRole = ref([
    {
      roleid: '',
      rolename: '',
    }
  ])
  const optionsMasterRole = ref([
    {
      value: '',
      text: '',
    }
  ])

  const getMasterRole = async () => {
    try {
      const res = await API.get('api/master_data/get_master_roles')
      dataMasterRole.value = res.data.results
      optionsMasterRole.value = dataMasterRole.value.map(function (item) {
        return {
          value: item.roleid,
          text: item.rolename,
        };
      });
    } catch (error: any) {
      console.log(error.response)
    }
  }



  onMounted(() => { });
  return {
    dataMasterRole,
    getMasterRole,
    optionsMasterRole
  }
}
