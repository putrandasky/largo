import {
  ref, reactive, nextTick
} from "vue";
export default function useGetData() {
  const isMounted = ref(false);
  const loading = ref(true);
  const table = ref({
    total: 644,
    data: []
  });
  const getData = async (apiMethods: any) => {
    loading.value = true
    try {
      const res = await apiMethods
      table.value.data = await res.data.results
      if (typeof res.data.total_data !== 'undefined') {
        table.value.total = +res.data.total_data
      }
      isMounted.value = true
      loading.value = false
    } catch (error) {
      loading.value = false
    }
  }
  const getDataV2 = async (apiMethods: any, data: any) => {
    loading.value = true
    try {
      const res = await apiMethods
      // Object.assign(data.value, {})
      Object.assign(data, await res.data.results)
      // data.value = await res.data.results
      nextTick()
      isMounted.value = true
      loading.value = false
    } catch (error) {
      loading.value = false
    }
  }
  // const pageChange = async (currentPage:Number, perPage:Number) => {
  //   page = currentPage
  //   offset = perPage
  //   loading.value = true
  //   await getNewData()
  //   loading.value = false
  // }
  return {
    isMounted,
    loading,
    table,
    getData,
    getDataV2
    // pageChange
  }
}
