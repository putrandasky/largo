import {
  defineComponent,
  onMounted,
  ref,
  reactive
} from 'vue';
import API from "@/core/services/ApiService";
export default function useMasterArea() {
  const dataMasterArea = ref([
    {
      areaid: '',
      areaname: '',
    }
  ])
  const optionsMasterArea = ref([
    {
      value: '',
      text: '',
    }
  ])

  const getMasterArea = async () => {
    try {
      const res = await API.get('api/master_data/get_master_area')
      dataMasterArea.value = res.data.results
      optionsMasterArea.value = dataMasterArea.value.map(function (item) {
        return {
          value: item.areaid,
          text: item.areaname,
        };
      });
    } catch (error: any) {
      console.log(error.response)
    }
  }



  onMounted(() => { });
  return {
    dataMasterArea,
    getMasterArea,
    optionsMasterArea
  }
}
