export default function colorBadge(data: any) {
    if (data == 'Open') {
        return 'info'
    }
    if (data == 'In Progress') {
        return 'success'
    }
    if (data == 'Close') {
        return 'secondary'
    }
    if (data == 'Waiting For Tally') {
        return 'info'
    }
    if (data == 'Tallying In Progress') {
        return 'success'
    }
    if (data == 'Completed') {
        return 'secondary'
    }
    if (data == 'NORMAL') {
        return 'primary'
    }
    return 'secondary'
}
