//@ts-nocheck
import {
  ElMessageBox,
  ElNotification
} from 'element-plus'
import i18n from '@/core/plugins/i18n'
const { t } = i18n.global
export default function useDeleteConfirm(items: any, id: any) {
  ElMessageBox.confirm(
    t('Are you sure want to delete this item?'),
    t('Warning'), {
    confirmButtonText: 'OK',
    cancelButtonText: 'Cancel',
    type: 'warning',
  }
  )
    .then(() => {
      let index = items.findIndex(data => data.itemid === id)
      items.splice(index, 1)
      ElNotification({
        title: t('Success'),
        message: t('This item successfuly deleted'),
        type: 'success',
      })
    })
    .catch(() => {
      // ElMessage({
      //   type: 'info',
      //   message: 'Delete canceled',
      // })
    })
}
