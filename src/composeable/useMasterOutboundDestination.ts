import {
  defineComponent,
  onMounted,
  ref,
  reactive
} from 'vue';
import { get } from "@/core/services/Axios";
export default function useMasterOutboundDestination() {
  const dataMasterDestination = ref([
    {
      destinationid: '',
      destinationname: '',
    }
  ])
  const optionsMasterDestination = ref([
    {
      value: '',
      text: '',
    }
  ])

  const getMasterOutboundDestination = async () => {
    try {
      const res = await get('api/master_data/get_master_destinations')
      dataMasterDestination.value = res.data.results
      optionsMasterDestination.value = dataMasterDestination.value.map(function (item) {
        return {
          value: item.destinationid,
          text: item.destinationname,
        };
      });
    } catch (error: any) {
      console.log(error.response)
    }
  }



  onMounted(() => { });
  return {
    dataMasterDestination,
    getMasterOutboundDestination,
    optionsMasterDestination
  }
}
