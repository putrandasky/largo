export default function useCheckSearch() {

    const checkSearch = (obj: any, search: any, isNumber = false) => {
        if (isNumber || obj === null) {
            return (search !== '' ? obj === search : true)
        }
        return obj.toLowerCase().indexOf(search.toLowerCase()) > -1
    }
    return {
        checkSearch
    }
}
