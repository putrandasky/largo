import {
  defineComponent,
  onMounted,
  ref,
  reactive
} from 'vue';
import API from "@/core/services/ApiService";
export default function useMasterUser() {
  const dataMasterUser = ref([
    {
      userid: '',
      username: '',
    }
  ])
  const optionsMasterUser = ref([
    {
      value: '',
      text: '',
    }
  ])

  const getMasterUser = async () => {
    try {
      const res = await API.get('api/master_data/get_master_users')
      dataMasterUser.value = res.data.results
      optionsMasterUser.value = dataMasterUser.value.map(function (item) {
        return {
          value: item.userid,
          text: item.username,
        };
      });
    } catch (error: any) {
      console.log(error.response)
    }
  }



  onMounted(() => { });
  return {
    dataMasterUser,
    getMasterUser,
    optionsMasterUser
  }
}
