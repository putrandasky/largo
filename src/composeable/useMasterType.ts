import {
  defineComponent,
  onMounted,
  ref,
  reactive
} from 'vue';
import API from "@/core/services/ApiService";
export default function useMasterType() {
  const dataMasterType = ref([
    {
      locationtypeid: '',
      locationtypename: '',
    }
  ])
  const optionsMasterType = ref([
    {
      value: '',
      text: '',
    }
  ])

  const getMasterType = async () => {
    try {
      const res = await API.get('api/master_data/get_master_type')
      dataMasterType.value = res.data.results
      optionsMasterType.value = dataMasterType.value.map(function (item) {
        return {
          value: item.locationtypeid,
          text: item.locationtypename,
        };
      });
    } catch (error: any) {
      console.log(error.response)
    }
  }



  onMounted(() => { });
  return {
    dataMasterType,
    getMasterType,
    optionsMasterType
  }
}
