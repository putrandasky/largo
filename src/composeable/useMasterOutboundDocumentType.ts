import {
  defineComponent,
  onMounted,
  ref,
  reactive
} from 'vue';
import { get } from "@/core/services/Axios";
export default function useMasterOutboundDocumentType() {
  const dataMasterDocumentType = ref([
    {
      documenttypeid: '',
      documenttypename: '',
    }
  ])
  const optionsMasterDocumentType = ref([
    {
      value: '',
      text: '',
    }
  ])

  const getMasterOutboundDocumentType = async () => {
    try {
      const res = await get('api/master_data/get_master_outboundTypes')
      dataMasterDocumentType.value = res.data.results
      optionsMasterDocumentType.value = dataMasterDocumentType.value.map(function (item) {
        return {
          value: item.documenttypeid,
          text: item.documenttypename,
        };
      });
    } catch (error: any) {
      console.log(error.response)
    }
  }



  onMounted(() => { });
  return {
    dataMasterDocumentType,
    getMasterOutboundDocumentType,
    optionsMasterDocumentType
  }
}
