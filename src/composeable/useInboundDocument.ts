import {
  defineComponent,
  onMounted,
  ref,
  reactive
} from 'vue';
import axios, { post } from "@/core/services/Axios";
export default function useInboundDocument() {
  const initialState = {
    warehouse_id: '0',
    page: '',
    offset: '',
    code: '',
    source: '',
    type: '',
    status: '',
    from: '',
    to: ''
  }
  const dataInboundDocument = ref([
    {
      id: '',
      code: '',
      source: '',
    }
  ])
  const dataInboundDocumentItem = ref([
    {
      detail: [],
      items: []
    }
  ])
  const optionsInboundDocument = ref([
    {
      value: '',
      text: '',
    }
  ])
  const optionsInboundDocument2 = ref([
    {
      value: '',
      label: '',
    }
  ])

  const getInboundDocument = async (state: any = null) => {

    try {
      let data = initialState
      if (state) {
        data = { ...initialState, ...state }
      }
      let dataString = new URLSearchParams(data)
      const res = await post('api/inbound/getList', dataString)
      dataInboundDocument.value = res.data.results
      optionsInboundDocument.value = dataInboundDocument.value.map(function (item) {
        return {
          value: item.id,
          text: `${item.code} - ${item.source}`,
        };
      });
      optionsInboundDocument2.value = dataInboundDocument.value.map(function (item) {
        return {
          value: item.id,
          label: `${item.code} - ${item.source}`,
        };
      });
    } catch (error: any) {
      console.log(error.response)
    }
  }
  const getInboundDocumentItem = async (id: any) => {
    try {
      let data = { id: id }
      let dataString = new URLSearchParams(data)
      const res = await post('api/inbound/getDetail', dataString)
      dataInboundDocumentItem.value = res.data.results
    } catch (error: any) {
      console.log(error.response)
    }
  }



  onMounted(() => { });
  return {
    dataInboundDocument,
    getInboundDocument,
    optionsInboundDocument,
    optionsInboundDocument2,
    getInboundDocumentItem,
    dataInboundDocumentItem
  }
}
