import {
  defineComponent,
  onMounted,
  ref,
  reactive
} from 'vue';
import API from "@/core/services/ApiService";
export default function useMasterCategories() {
  const dataMasterCategories = ref([
    {
      categoryid: '',
      categoryname: '',
    }
  ])
  const optionsMasterCategories = ref([
    {
      value: '',
      text: '',
    }
  ])

  const getMasterCategories = async () => {
    try {
      const res = await API.get('api/master_data/get_master_category')
      dataMasterCategories.value = res.data.results
      optionsMasterCategories.value = dataMasterCategories.value.map(function (item) {
        return {
          value: item.categoryid,
          text: item.categoryname,
        };
      });
    } catch (error: any) {
      console.log(error.response)
    }
  }



  onMounted(() => { });
  return {
    dataMasterCategories,
    getMasterCategories,
    optionsMasterCategories
  }
}
