import {
  defineComponent,
  onMounted,
  ref,
  reactive
} from 'vue';
import { get } from "@/core/services/Axios";
export default function useMasterBinLocation() {
  const dataMasterBinLocation = ref([
    {
      locationid: '',
      locationname: '',
    }
  ])
  const optionsMasterBinLocation = ref([
    {
      value: '',
      text: '',
    }
  ])

  const getMasterBinLocation = async () => {
    try {
      const res = await get('api/master_data/get_master_binLocation')
      dataMasterBinLocation.value = res.data.result
      optionsMasterBinLocation.value = dataMasterBinLocation.value.map(function (item) {
        return {
          value: item.locationid,
          text: item.locationname,
        };
      });
    } catch (error: any) {
      console.log(error.response)
    }
  }



  onMounted(() => { });
  return {
    dataMasterBinLocation,
    getMasterBinLocation,
    optionsMasterBinLocation
  }
}
