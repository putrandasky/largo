import {
  defineComponent,
  onMounted,
  ref,
  reactive
} from 'vue';
import API from "@/core/services/ApiService";
export default function useMasterUom() {
  const dataMasterUom = ref([
    {
      unitid: '',
      unitname: '',
    }
  ])
  const optionsMasterUom = ref([
    {
      value: '',
      text: '',
    }
  ])

  const getMasterUom = async () => {
    try {
      const res = await API.get('api/master_data/get_master_unitOfmeasurement')
      dataMasterUom.value = res.data.results
      optionsMasterUom.value = dataMasterUom.value.map(function (item) {
        return {
          value: item.unitid,
          text: item.unitname,
        };
      });
    } catch (error: any) {
      console.log(error.response)
    }
  }



  onMounted(() => { });
  return {
    dataMasterUom,
    getMasterUom,
    optionsMasterUom
  }
}
