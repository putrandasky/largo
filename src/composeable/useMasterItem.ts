import {
  defineComponent,
  onMounted,
  ref,
  reactive
} from 'vue';
import API from "@/core/services/ApiService";
export default function useMasterItem() {
  const dataMasterItem = ref([
    {
      itemid: '',
      itemname: '',
      itemcode: '',
      uomname: '',
    }
  ])
  const optionsMasterItem = ref([
    {
      value: '',
      text: '',
      uom: ''
    }
  ])
  const optionsMasterItem2 = ref([
    {
    }
  ])

  const getMasterItem = async () => {
    try {
      const res = await API.get('api/master_data/get_master_item')
      dataMasterItem.value = res.data.results
      optionsMasterItem.value = dataMasterItem.value.map(function (item) {
        return {
          value: item.itemid,
          text: `${item.itemcode} - ${item.itemname}`,
          uom: item.uomname,
        };
      });
      optionsMasterItem2.value = dataMasterItem.value.map(function (item) {
        return {
          value: item.itemid,
          label: item.itemname,
          uom: item.uomname,
        };
      });
    } catch (error: any) {
      console.log(error.response)
    }
  }



  onMounted(() => { });
  return {
    dataMasterItem,
    getMasterItem,
    optionsMasterItem,
    optionsMasterItem2
  }
}
