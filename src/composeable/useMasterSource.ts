import {
  defineComponent,
  onMounted,
  ref,
  reactive
} from 'vue';
import { get } from "@/core/services/Axios";
export default function useMasterSource() {
  const dataMasterSource = ref([
    {
      sourceid: '',
      sourcename: '',
    }
  ])
  const optionsMasterSource = ref([
    {
      value: '',
      text: '',
    }
  ])

  const getMasterSource = async () => {
    try {
      const res = await get('api/master_data/get_master_sources')
      dataMasterSource.value = res.data.results
      optionsMasterSource.value = dataMasterSource.value.map(function (item) {
        return {
          value: item.sourceid,
          text: item.sourcename,
        };
      });
    } catch (error: any) {
      console.log(error.response)
    }
  }



  onMounted(() => { });
  return {
    dataMasterSource,
    getMasterSource,
    optionsMasterSource
  }
}
