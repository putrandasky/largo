import {
    ref,
} from 'vue';
import dayjs from 'dayjs'

export default function useCalendarFilterHandler(from: any, to: any) {
    const calendar = ref(null)
    const onDateChange = (val: any) => {
        from.value = dayjs(val[0]).format('YYYY-MM-DD')
        to.value = dayjs(val[1]).format('YYYY-MM-DD')

    }
    return {
        onDateChange,
        calendar
    }
}
