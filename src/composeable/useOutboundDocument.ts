import {
  defineComponent,
  onMounted,
  ref,
  reactive
} from 'vue';
import axios, { post } from "@/core/services/Axios";
export default function useOutboundDocument() {
  const initialState = {
    warehouse_id: '0',
    page: '',
    offset: '',
    code: '',
    destination: '',
    type: '',
    status: '',
    from: '',
    to: ''
  }
  const dataOutboundDocument = ref([
    {
      id: '',
      code: '',
      destination: '',
    }
  ])
  const dataOutboundDocumentItem = ref([
    {
      detail: [],
      items: []
    }
  ])
  const optionsOutboundDocument = ref([
    {
      value: '',
      text: '',
    }
  ])

  const getOutboundDocument = async (state: any = null) => {

    try {
      let data = initialState
      if (state) {
        data = { ...initialState, ...state }
      }
      let dataString = new URLSearchParams(data)
      const res = await post('api/outbound/getList', dataString)
      dataOutboundDocument.value = res.data.results
      optionsOutboundDocument.value = dataOutboundDocument.value.map(function (item) {
        return {
          value: item.id,
          text: `${item.code} - ${item.destination}`,
        };
      });
    } catch (error: any) {
      console.log(error.response)
    }
  }
  const getOutboundDocumentItem = async (id: any) => {
    try {
      let data = { id: id }
      let dataString = new URLSearchParams(data)
      const res = await post('api/outbound/getDetail', dataString)
      dataOutboundDocumentItem.value = res.data.results
    } catch (error: any) {
      console.log(error.response)
    }
  }



  onMounted(() => { });
  return {
    dataOutboundDocument,
    getOutboundDocument,
    optionsOutboundDocument,
    getOutboundDocumentItem,
    dataOutboundDocumentItem
  }
}
