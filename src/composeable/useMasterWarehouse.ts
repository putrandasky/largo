import {
  defineComponent,
  onMounted,
  ref,
  reactive
} from 'vue';
import API from "@/core/services/ApiService";
export default function useMasterWarehouse() {
  const dataMasterWarehouse = ref([
    {
      warehouseid: '',
      warehousename: '',
    }
  ])
  const optionsMasterWarehouse = ref([
    {
      value: '',
      text: '',
    }
  ])

  const getMasterWarehouse = async () => {
    try {
      const res = await API.get('api/master_data/get_master_warehouse')
      dataMasterWarehouse.value = res.data.results
      optionsMasterWarehouse.value = dataMasterWarehouse.value.map(function (item) {
        return {
          value: item.warehouseid,
          text: item.warehousename,
        };
      });
    } catch (error: any) {
      console.log(error.response)
    }
  }



  onMounted(() => { });
  return {
    dataMasterWarehouse,
    getMasterWarehouse,
    optionsMasterWarehouse
  }
}
