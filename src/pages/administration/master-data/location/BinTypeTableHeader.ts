const tableHeader = [
    {
        name: "No",
        key: "no",
        class: "text-center",
        thStyle: {
            minWidth: '30px',
            width: '30px',
        },
    },
    {
        name: "Location Type Name",
        key: "locationtypename",
        sortable: true,
        thStyle: {
            minWidth: "100px",
        },
    },
    {
        name: "Location Type Description",
        key: "locationtypedescription",
        sortable: true,
        thStyle: {
            minWidth: "100px",
        },
    },
    {
        name: "",
        key: "action",
        thStyle: {
            minWidth: "100px",
            width: "100px",
        },
    },
]

export default tableHeader;