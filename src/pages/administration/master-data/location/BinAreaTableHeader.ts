const tableHeader = [
    {
        name: "No",
        key: "no",
        class: "text-center",
        thStyle: {
            minWidth: '30px',
            width: '30px',
        },
    },
    {
        name: "Area Name",
        key: "areaname",
        sortable: true,
        thStyle: {
            minWidth: "100px",
        },
    },
    {
        name: "Area Description",
        key: "areadescription",
        sortable: true,
        thStyle: {
            minWidth: "100px",
        },
    },
    {
        name: "",
        key: "action",
        thStyle: {
            minWidth: "100px",
            width: "100px",
        },
    },
]

export default tableHeader;