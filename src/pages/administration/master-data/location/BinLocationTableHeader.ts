const tableHeader = [
    {
        name: "No",
        key: "no",
        class: "text-center",
        thStyle: {
            minWidth: '30px',
            width: '30px',
        },
    },
    {
        name: "Bin Name",
        key: "locationname",
        sortable: true,
        thStyle: {
            minWidth: "100px",
        },
    },
    {
        name: "Bin Description",
        key: "locationdescription",
        sortable: true,
        thStyle: {
            minWidth: "100px",
        },
    },
    {
        name: "Bin Category",
        key: "categoryname",
        sortable: true,
        thStyle: {
            minWidth: "100px",
        },
    },
    {
        name: "Bin Type",
        key: "typename",
        sortable: true,
        thStyle: {
            minWidth: "100px",
        },
    },
    {
        name: "Bin Area",
        key: "areaname",
        sortable: true,
        thStyle: {
            minWidth: "100px",
        },
    },
    {
        name: "",
        key: "action",
        thStyle: {
            minWidth: "100px",
            width: "100px",
        },
    },
]

export default tableHeader;