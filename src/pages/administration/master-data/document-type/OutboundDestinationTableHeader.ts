const tableHeader = [
    {
        name: "No",
        key: "no",
        class: "text-center",
        thStyle: {
            minWidth: '30px',
            width: '30px',
        },
    },
    // {
    //     name:"Code",
    //     key:"code",
    //     sortable:true,
    //     thStyle: {
    //         minWidth: "200px",
    //         width: '200px',
    //       },
    // },
    {
        name: "Name",
        key: "destinationname",
        sortable: true,
        thStyle: {
            minWidth: "350px",
            width: '350px',
        },
    },
    // {
    //     name:"Phone",
    //     key:"phone",
    //     sortable:true,
    //     thStyle: {
    //         minWidth: "200px",
    //         width: '200px',
    //               },
    // },
    // {
    //     name:"CP",
    //     key:"cp",
    //     sortable:true,
    //     thStyle: {
    //         minWidth: "200px",
    //         width: '200px',
    //               },
    // },
    // {
    //     name:"Email",
    //     key:"email",
    //     sortable:true,
    //     thStyle: {
    //         minWidth: "200px",
    //         width: '200px',
    //       },
    // },
    {
        name: "Address",
        key: "destinationaddress",
        sortable: true,
        thStyle: {
            minWidth: "300px",
        },
    },
    {
        name: "",
        key: "action",
        thStyle: {
            minWidth: "100px",
            width: "100px",
        },
    },
]

export default tableHeader;