const tableHeader = [
    {
        name: "No",
        key: "no",
        class: "text-center",
        thStyle: {
            minWidth: '30px',
            width: '30px',
        },
    },
    {
        name: "Code",
        key: "sourcecategorycode",
        sortable: true,
        thStyle: {
            minWidth: "100px",
        },
    },
    {
        name: "Name",
        key: "sourcecategoryname",
        sortable: true,
        thStyle: {
            minWidth: "100px",
        },
    },
    {
        name: "",
        key: "action",
        thStyle: {
            minWidth: "100px",
            width: "100px",
        },
    },
]

export default tableHeader;