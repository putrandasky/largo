import i18n from '@/core/plugins/i18n'
const { t } = i18n.global
const tableHeader = [
  {
    name: 'No',
    key: 'no',
    class: 'text-center',
    thStyle: {
      minWidth: '30px',
      width: '30px',
    },
  },
  {
    name: t('code'),
    key: 'itemcode',
    thStyle: {
      minWidth: '125px',
      width: '125px',
    },
  },
  {
    name: t('item'),
    key: 'itemname',
  },
  {
    name: t('qtyMin'),
    key: 'minimumqty',
    sortable: true,
    thStyle: {
      minWidth: '120px',
      width: '120px',
    },
  },
  {
    name: t('qtyDefault'),
    key: 'defaultqty',
    sortable: true,
    thStyle: {
      minWidth: '150px',
      width: '120px',
    },
  },
  // {
  //   name: "Weight",
  //   key: "weight",
  //   sortable:true,
  //   thStyle: {
  //     minWidth: "100px",
  //     width: "100px",
  //   },
  // },
  // {
  //   name: "Volume",
  //   key: "volume",
  //   sortable:true,
  //   thStyle: {
  //     minWidth: "100px",
  //     width: "100px",
  //   },
  // },
  // { name: "Strategy", key: "pickingStrategy" },
  // { name: "Period", key: "pickingPeriod" },
  {
    name: '',
    key: 'action',
    thStyle: {
      minWidth: '100px',
      width: '100px',
    },
  },
]

export default tableHeader
