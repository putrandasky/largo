import i18n from '@/core/plugins/i18n'
const { t } = i18n.global
const tableHeader = [
    {
        name: "No",
        key: "no",
        class: "text-center",
        thStyle: {
            minWidth: '30px',
            width: '30px',
        },
    },
    {
        name: t("code"),
        key: "unitcode",
        sortable: true,
        thStyle: {
            minWidth: "100px",
            width: "100px",
        },
    },
    {
        name: t("name"),
        key: "unitname",
        sortable: true,
        thStyle: {
            minWidth: "100px",
        },
    },
    {
        name: "",
        key: "action",
        thStyle: {
            minWidth: "100px",
            width: "100px",
        },
    },
]

export default tableHeader;