const tableHeader = [
    {
        name: "No",
        key: "no",
        class: "text-center",
        thStyle: {
            minWidth: '30px',
            width: '30px',
        },
    },
    // {
    //     name:"Code",
    //     key:"usernik",
    //     sortable:true,
    //     thStyle: {
    //         minWidth: "200px",
    //         width: '200px',
    //       },
    // },
    {
        name: "Role Name",
        key: "rolename",
        sortable: true,

    },
    {
        name: "Role Description",
        key: "roledescription",
        sortable: true,

    },

    {
        name: "",
        key: "action",
        thStyle: {
            minWidth: "100px",
            width: "100px",
        },
    },
]

export default tableHeader;