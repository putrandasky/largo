const tableHeader = [
    {
        name: "No",
        key: "no",
        class: "text-center",
        thStyle: {
            minWidth: '30px',
            width: '30px',
        },
    },
    // {
    //     name:"Code",
    //     key:"usernik",
    //     sortable:true,
    //     thStyle: {
    //         minWidth: "200px",
    //         width: '200px',
    //       },
    // },
    {
        name: "Name",
        key: "fullname",
        sortable: true,

    },
    {
        name: "Username",
        key: "username",
        sortable: true,

    },
    {
        name: "Role",
        key: "userroles",
        sortable: true,
        thStyle: {
            minWidth: "200px",
            width: '200px',
        },
    },
    {
        name: "Status",
        key: "status",
        thStyle: {
            minWidth: "100px",
            width: '100px',
        },
    },
    {
        name: "",
        key: "action",
        thStyle: {
            minWidth: "100px",
            width: "100px",
        },
    },
]

export default tableHeader;