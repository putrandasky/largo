import {
  defineComponent,
  onMounted,
  ref,
  reactive
} from 'vue';
import API from "@/core/services/ApiService";
export default function getDashboardData() {
  const data = ref({
    warehouseCapacity: {
      max_loc: 0,
      registered_loc: 0,
      filled_loc: 0,
      checked_loc: 0
    }
  })
  const loadedState = ref({
    warehouseCapacity: false
  })
  const getWarehouseCapacityData = async () => {
    try {
      const res = await API.get('api/dashboard/getWarehouseCapacityWidget')
      data.value.warehouseCapacity = res.data.results
      loadedState.value.warehouseCapacity = true
      console.log(res);
    } catch (error: any) {
      console.log(error.response);
    }
  }
  onMounted(() => { });
  return {
    data,
    loadedState,
    getWarehouseCapacityData
  };
}
