# 18-June-22

## 0.12

### ADD

- Search result page
- Search select in header

## 0.11

### ADD

- Input Select in dropdown warehouse at topbar
- Dropdown user profile
- User Signin
- User Signup

# 1-June-22

## 0.10

### ADD

- Pages: Visualitation for active location

# 1-June-22

## 0.9.2

### ADD

- Pages: Create New Cycle Count Jobs, Create New Bin Transfer Jobs
- composeable usemasteruser

# 31-May-22

## 0.9.1

### ADD

- Pages: Active inventory detail, Active Location detail, cycle count detail, bin transfer detail, quarantine detail

# 22-May-22

## 0.9

### ADD

- Pages : Cycle Count, Expired Item List, Item Transfer, Material In, material Out
- Component Widgets : Button Submit, buton back, Button add new, btn tool, sub text

### REVISE

- Replenishment Index

# 11-May-22

## 0.8.1

### ADD

- Loading Jobs Create Page

# 30-Apr-22

## 0.8

### IMPROVE

- fixing Inbound receiving detail item in create page

### ADD

- Oubound document create
- Outbound picking jobs create
- composeable for outbound document

# 13-Apr-22

## 0.7

### ADD

- Inbound document detail
- inbound receivings detail
- inbound Outbound detail
- Widgets for DocumentInfoArea
- components back button
- components filterable select
- api in inbound and outbound

### REVISE

- Inbound document create page
- inbound receivings create page

### IMPROVE

- fallback dateformat value

# 11-Apr-22

## 0.6.13

### ADD

- inbound document create page
- inbound reveivings create page

### IMPROVE

- Filtering date with DD-MM-YY

# 28-Mar-22

## 0.6.12

### FIXED

- Filter oubtound and receiving improvement

## 0.6.11

### ADD

- Add API for Outbound documents
- composable for calendar handler and outbound destination

# 22-Mar-22

## 0.6.10

### ADD

- Add API for inbound documents and receiving jobs index view

# 17-Mar-22

## 0.6.9

### ADD

- Add router for Operation page
- Operation index page view

# 13-Mar-22

## 0.6.8

### ADD

- Add router for Inbound & Outbound page
- Inbound & Outbound index page view
- Composable for color badge, delete confirm, getdata, and some master data
- Delete Confirmation in Master Data Item

# 2-Mar-22

## 0.6.7

### FIXED

- pagination error due to null data
- data in mins instead in hrs

## 0.6.6

### ADD

- Loader and animation

### CHANGE

- api for dashboard operator performance KPI

# 21-Feb-22

## 0.6.5

### ADD

- dynamic calendar in dashboard

# 11-Feb-22

## 0.6.4

### CHANGE

- Refactor table component
- data structure in dashboard

# 6-Feb-22

## 0.6.3

### CHANGE

- Table data in Item master with on premise

# 3-Feb-22

## 0.6.2

### ADD

- Instant Search and Filter (Item Master)
- Hightlight the item name while search

### CHANGE

- component input and select change v-model:value to v-model
- layout instant search and filter

# 1-Feb-22

## 0.6.1

### IMPROVE

- Responsiveness Dashboard Page

# 31-Jan-22

## 0.6

### ADD

- Date Range Calendar for Operation Act Dashboard
- Limit Size and Accept file for upload file in item master

# 26-Jan-22

## 0.5

### ADD

- Page Add New for Master Location, Document Type, User Management, Role Management
- Composable File for Master Area, Categories, Role, Source Type, type, Warehouse
- placeholder props for component select

### CHANGE

- Remodule for Management route, Document type route

# 25-Jan-22

## 0.4

### ADD

- Integrate API Dashboard
- Refactoring and widget Dashboard

### CHANGE

- Page Add New Master Item

# 23-Jan-22

## 0.3

### ADD

- Dashboard
- Page Add new in Master item
- Page User Management
- Integrate API for Administration
- User Management pages
- Refactoring Route
- Provide Internasionalisation

### IMPROVE

- Title and subtitle as meta data in route

# 17-Jan-22

## 0.2

### ADD

- Page Master Location
- Page Master Documente type
- Widget Search
- Custom filter

### IMPROVE

- Provide custom plugin fOr components (input, select, dropdown) & layout (col,row)
- Provide custom widget (searchfield, etc)
- Using Vite instead VueCli

### FIXED

- datatables Keen themes (add index, change page show all data, change row per page not work)

# 10-Jan-22

## 0.1

### ADD

- Dashboard Page
- Master Item Page
- Renaming Sidebar
