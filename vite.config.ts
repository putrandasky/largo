import { defineConfig, loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'
import alias from '@rollup/plugin-alias'
import { resolve } from 'path'
const projectRootDir = resolve(__dirname);
// https://vitejs.dev/config/
export default ({ mode }) => {
  process.env = { ...process.env, ...loadEnv(mode, process.cwd()) };
  return defineConfig({
    plugins: [alias(), vue()],
    build: {
      sourcemap: false,
    },
    resolve: {
      alias: {
        "@": resolve(projectRootDir, "src"),
        "~": resolve(projectRootDir, "node_modules")
        // "vue": resolve(projectRootDir, "node_modules/vue/dist/vue.runtime.esm-browser.js")
      },
    },

  })
}